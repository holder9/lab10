/*
 * Daniel Herold
 * Avery Holder
 * CPSC 2151 - 002
 * Lab 10 - ModelViewControllerGui
 * 19 April 2019
 */
 
package cpsc2150.mortgages;

/**
 * @invariant 0 < costOfHome
 * @invariant 0 <= downPayment < costOfHome
 * @invariant MIN_YEARS <= numYears <= MAX_YEARS
 * @invariant 0 <= rate <= 1
 * @invariant 0 < debtToIncomeRatio
 * @invariant MIN_YEARS * MONTHS_IN_YEAR <= NumberOfPayments <= MAX_YEARS * MONTHS_IN_YEAR
 * @invariant 0 < principal
 * @invariant 0 <= percentDown < 1
 * Correspondence Payment = payment
 * Correspondence Rate = rate
 * Correspondence Customer = customer
 * Correspondence DebtToIncomeRatio = debtToIncomeRatio
 * Correspondence Principal = Principal
 * Correspondence NumberOfPayments = numberOfPayments
 * Correspondence PercentDown = percentDown
 */
public class Mortgage extends AbsMortgage implements IMortgage{
    private double costOfHome;
    private double downPayment;
    private int numYears;
    private ICustomer customer;
    private double rate;
    private double payment;
    private double principal;
    private double debtToIncomeRatio;
    private double numberOfPayments;
    private double percentDown;
    private int MONTHS_IN_YEAR = 12;

    /**
     * @param costOfHome the cost of the home
     * @param downPayment the down payment offered by customer
     * @param numYears the number of years to pay off loan
     * @param customer the customer awaiting loan approval
     * @pre [must have instance of customer class before calling mortgage]
     *         [cost of home > 0 and downPayment >= 0 and numYears > 0]
     * @post [creates instance of mortgage class]
     */
    public Mortgage(double costOfHome, double downPayment, int numYears, ICustomer customer) {
        this.costOfHome = costOfHome;
        this.downPayment = downPayment;
        this.numYears = numYears;
        this.customer = customer;
        percentDown = downPayment/costOfHome;
        principal = costOfHome - downPayment;
        numberOfPayments = MONTHS_IN_YEAR * numYears;
        rate = calcAPRRate()/MONTHS_IN_YEAR;
        payment = (rate*principal)/(1-Math.pow(1+rate,-numberOfPayments));
        debtToIncomeRatio = (customer.getMonthlyDebtPayments() + payment)/(customer.getIncome()/MONTHS_IN_YEAR);
    }

    /**
     * @return the calculatedAPR rate for the current home and customer
     * @post calcAPRRate >= .005 and calcAPRRate <= .14
     */
    private double calcAPRRate(){
        double rateAPR = BASERATE;
        if(numYears < MAX_YEARS)
            rateAPR += GOODRATEADD;
        else
            rateAPR += NORMALRATEADD;
        if(percentDown < PREFERRED_PERCENT_DOWN)
            rateAPR += GOODRATEADD;
        if(customer.getCreditScore() < BADCREDIT)
            rateAPR += VERYBADRATEADD;
        else if(customer.getCreditScore() < FAIRCREDIT)
            rateAPR += BADRATEADD;
        else if(customer.getCreditScore() < GOODCREDIT)
            rateAPR += NORMALRATEADD;
        else if(customer.getCreditScore() < GREATCREDIT)
            rateAPR += GOODRATEADD;
        return rateAPR;
    }

    public boolean loanApproved(){
        return(getRate() < RATETOOHIGH && percentDown >= MIN_PERCENT_DOWN && debtToIncomeRatio <= DTOITOOHIGH);
    }

    public double getPayment(){
        return payment;
    }

   public double getRate(){
        return rate * MONTHS_IN_YEAR;
    }

    public double getPrincipal(){
        return principal;
    }

    public int getYears(){
        return numYears;
    }

}
