/*
 * Daniel Herold
 * Avery Holder
 * CPSC 2151 - 002
 * Lab 10 - ModelViewControllerGui
 * 19 April 2019
 */

package cpsc2150.mortgages;

public class MortgageController implements IMortgageController {

    private IMortgageView view;

    public MortgageController(IMortgageView v) {
        view = v;
    }

    public void submitApplication() {

        //Variables
        ICustomer customer = new Customer(0,1,1,"Default");
        IMortgage mortgage = new Mortgage(1, 0.5, 10, customer);
        double income;
        String name;
        double debtPayment;
        int creditScore;
        double houseCost;
        double downPayment;
        int years;
        String error;

        //Get the first four variables from the user. These specifically relate to data about the customer.
        name = view.getName();
        income = view.getYearlyIncome();
        debtPayment = view.getMonthlyDebt();
        creditScore = view.getCreditScore();
        error = "";

        //Perform error checking on the variables. If the variables don't meet preconditions, set error to a message
        //The message will be displayed at the bottom.
        if(income <= 0)
            error = "Income must be greater than 0.";
        else if(debtPayment < 0)
            error = "Debt must be greater than or equal to 0.";
        else if(creditScore < 0 || creditScore > 850)
            error = "Credit Score must be greater than 0 and less than 850";
        else
            customer = new Customer(debtPayment, income, creditScore, name);

        //Get the next three variables from the user. These have to do specifically with the mortgage.
        houseCost = view.getHouseCost();
        downPayment = view.getDownPayment();
        years = view.getYears();

        //Perform error checking on the variables. If the variables don't meet preconditions, set error to a message
        //The message will be displayed at the bottom.
        if(houseCost <= 0)
            error = "Cost must be greater than 0.";
        else if(downPayment < 0 || downPayment > houseCost)
            error = "Down Payment must be greater than 0 and less than the cost of the house.";
        else if(years < 0)
            error = "Years must be greater than 0.";
        else
            mortgage = new Mortgage(houseCost, downPayment, years, customer);

        //Error is "" by default, so if there is no error, the message disappears.
        view.printToUser(error);

        //Display rates to the user, assuming there are no errors in the input
        if(error.equals("")) {
            view.displayApproved(mortgage.loanApproved());
            view.displayRate(mortgage.getRate());
            view.displayPayment(mortgage.getPayment());
        }
    }
}
