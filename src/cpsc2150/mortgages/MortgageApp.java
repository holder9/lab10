/*
 * Daniel Herold
 * Avery Holder
 * CPSC 2151 - 002
 * Lab 10 - ModelViewControllerGui
 * 19 April 2019
 */
package cpsc2150.mortgages;
public class MortgageApp {
    public static void main(String [] args)
    {
        IMortgageView view = new MortgageView();
        IMortgageController controller = new MortgageController(view);
        view.setController(controller);
    }
}
